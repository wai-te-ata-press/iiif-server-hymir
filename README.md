# Iiif Server Hymir

Our IIIF server for the TIFFs of the Chinese sorts.
Software is https://github.com/dbmdz/iiif-server-hymir

# running a detached server using volume flipped_tiff
```
podman login registry.gitlab.com
podman pull registry.gitlab.com/wai-te-ata-press/iiif-server-hymir
podman run -d -p 9000:9000 -p 9001:9001 --rm -v flipped_tiff:/data registry.gitlab.com/wai-te-ata-press/iiif-server-hymir
```
