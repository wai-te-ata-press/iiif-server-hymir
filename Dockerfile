FROM registry.hub.docker.com/library/maven:3-jdk-11
RUN  apt-get update && \
    apt-get install -y \
        libopenjp2-7 \
        libturbojpeg0 \
        nano
WORKDIR /root
RUN curl -LOsS https://github.com/dbmdz/iiif-server-hymir/releases/download/4.1.3/hymir-4.1.3-exec.jar
COPY . .
EXPOSE 9000 9001
CMD java -jar hymir-4.1.3-exec.jar --spring.config.additional-location=file:rules.yml
